import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IFireDataSheet } from './interface';


@Injectable({
  providedIn: 'root'
})
export class RecieveDatafromFirebaseService {

  constructor(private realtimeDB: AngularFireDatabase) { }

  fetchDataSheetFromFirebase(): Observable<any> {
    return this.realtimeDB
                .list<IFireDataSheet>('/Mastersheet')
                .snapshotChanges().pipe(
                  map((products: any[]) => products.map(prod => {
                    const payload = prod.payload.val();
                    //console.log(payload);
                    const key = prod.key;
                    //console.log(key);
                    return <any>{ key, ...payload };
                  })),
                );
  }

}
