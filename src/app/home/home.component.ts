import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { StoreDatatoLocalstorageService } from '../store-datato-localstorage.service';
import { StudentData } from '../interface'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private fb: FormBuilder, private service: StoreDatatoLocalstorageService) {}


  ngOnInit() {
    debugger;
    //once page loaded, first data has to retrive from storage and show it in view/table
    this.retriveData();

  }


  isEdit = false;
  userSubmitted = false;
  studentList: StudentData[] = [];



    studentForm = this.fb.group({
    iD: ['', Validators.required],
    studentName: [null, Validators.required],
    fatherName: [null, Validators.required],
    email: [null, [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6)]],
    confirmPassword: ['', [Validators.required, Validators.minLength(6)]],
    mobileNumber: [null, [Validators.required, Validators.minLength(10)]]
  }, {validators: this.passwordMatchingValidator});


   passwordMatchingValidator(group:FormGroup) {
    return group.get('password')?.value === group.get('confirmPassword')?.value ? null :  { notmatched: true }
  }

      //Getter Methods for all our Form Controls....

      get studentName() {
        return this.studentForm.get('studentName') as FormControl;
      }

      get fatherName() {
        return this.studentForm.get('fatherName') as FormControl;
      }

      get email() {
        return this.studentForm.get('email') as FormControl;
      }

      get password() {
        return this.studentForm.get('password') as FormControl;
      }

      get confirmPassword() {
        return this.studentForm.get('confirmPassword') as FormControl;
      }

      get mobileNumber() {
        return this.studentForm.get('mobileNumber') as FormControl;
      }

    // ******************End of Getter Method for Form Controls...



  submitData() {
    debugger;
    // console.log(this.studentForm.value);

    this.userSubmitted = true;

    if (this.studentForm.valid){


    let _data:StudentData = this.studentForm.value;
    //console.log(_data);

    if (!this.isEdit) {
      this.service.setData(_data);
      alert("Created successfully!");

    } else {
     this.service.updateData(_data);
      // this.isEdit = false;
    }

    this.closePop();
    //Once submit data, form has to reset to empty.
    this.studentForm.reset();
    this.userSubmitted = false;

    debugger;
    //Once submit data, data has to store in local also view in table/userview.
    this.retriveData();

    }
  }


// To recive data from local storage to table.
  retriveData() {
    debugger;
    this.studentList = this.service.getData()

  }


  isPopOpen = false;

// First create form data, once modal opened.
  closePop() {
    this.isPopOpen = false;
  }

createData() {
debugger;
this.isPopOpen = true;
this.isEdit = false;
  let desList =  this.service.getData();
  let iD =1;

  if (desList && desList.length>0) {
    let list= desList.sort(function(a, b){return b.iD-a.iD});
        iD=list[0].iD+1;
  }

this.studentForm.patchValue({
  iD: iD ,
  studentName:null,
  fatherName: null,
  email: null,
  password: '',
  confirmPassword: '',
  mobileNumber: null
});

}


// to select the particular ID and pop up the data in modal.
  editData(iD:any) {

    this.isPopOpen = true;
    this.isEdit=true;

    for (var i = 0; i < this.studentList.length; i++) {

      if (iD == this.studentList[i].iD) {

        this.studentForm.patchValue({

          // set the value to the form once edit button clicked
          iD: this.studentList[i].iD,
          studentName: this.studentList[i].studentName,
          fatherName: this.studentList[i].fatherName,
          email: this.studentList[i].email,
          password: this.studentList[i].password,
          confirmPassword: this.studentList[i].confirmPassword,
          mobileNumber: this.studentList[i].mobileNumber
        });
   }
  }

}

  deleteData(index:number) {

    if(confirm("Are you sure to delete?")) {
      this.service.deleteSelectedRow(index);
      this.retriveData();
    }

  }

}

