

export interface StudentData {
    iD: number ;
    studentName: string | null;
    fatherName: string | null;
    email: string | null;
    password: number;
    confirmPassword: number;
    mobileNumber: number;
}

export interface IFireDataSheet {
  Employee_Id: number;
  FirstName: string | null;
  LastName: string | null;
}
