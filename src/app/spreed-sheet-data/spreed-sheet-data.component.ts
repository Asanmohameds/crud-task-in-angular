import { Component, OnInit } from '@angular/core';
import { RecieveDatafromFirebaseService } from '../recieve-datafrom-firebase.service'

import { IFireDataSheet } from '../interface';

@Component({
  selector: 'app-spreed-sheet-data',
  templateUrl: './spreed-sheet-data.component.html',
  styleUrls: ['./spreed-sheet-data.component.css']
})
export class SpreedSheetDataComponent implements OnInit {

  fetchedData: IFireDataSheet[];

  constructor(public fetchDataSheet: RecieveDatafromFirebaseService) { }

  ngOnInit(): void {

     this.fetchDataSheet.fetchDataSheetFromFirebase().subscribe((x) => {
      console.log(x);
      this.fetchedData = x;
    })
    //console.log(this.fetchedData);
  }

}
