import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SpreedSheetDataComponent } from './spreed-sheet-data/spreed-sheet-data.component';

const appRoutes: Routes = [
    {path:'home', component:HomeComponent},
    {path:'datasheet', component: SpreedSheetDataComponent},
    {path:'', redirectTo:'/home', pathMatch:'full'}
];

@NgModule({
imports: [RouterModule.forRoot(appRoutes,{useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
